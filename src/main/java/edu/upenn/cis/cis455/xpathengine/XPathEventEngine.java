package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XPathEventEngine implements XPathEngine {
    private static Logger logger = LogManager.getLogger(XPathEventEngine.class);

    String[] expressions;
    String path = "";
    List<boolean[]> matches = new ArrayList<>();
    MatchingStages[] xpaths;

    class MatchingStages {

        class XSubPath {
            OccurrenceEvent.Type type;
            String value;
            public XSubPath(OccurrenceEvent.Type type, String value) {
                this.type = type;
                this.value = value;
            }
            public boolean match(OccurrenceEvent event, boolean caseSensitivity) {
                if (!this.type.equals(event.getType())) {
                    return false;
                }
                
                if (OccurrenceEvent.Type.Open.equals(this.type)) {
                    if (!caseSensitivity) {
                        return this.value.toLowerCase().equals(event.getValue().toLowerCase());
                    }
                    return this.value.equals(event.getValue());
                }
                
                if (!OccurrenceEvent.Type.Text.equals(this.type)) {
                    return false;
                }
                
                // This is a Text test. 
                String test = new String(this.value);
                if (!(test.startsWith("[") && test.endsWith("]"))) {
                    // Test is not surrounded by []. 
                    return false;
                }
                
                test = test.substring(1, test.length() - 1).strip(); // text() = "..." or contains(text(), "...")
                if (test.startsWith("text()")) {
                    // text() = ...
                    test = test.substring(test.indexOf("=") + 1).strip(); // "..."
                    if (!(test.startsWith("\"") && test.endsWith("\""))) {
                        // String not surrounded by quotes. 
                        return false;
                    }
                    test = test.substring(1, test.length() - 1); // ...
                    if (test.equals(event.getValue())) {
                        // Exact match of content. 
                        return true;
                    }
                } else if (test.startsWith("contains(") && test.endsWith(")")) {
                    // contains(text(), "...")
                    test = test.substring(9, test.length() - 1).strip();
                    if (test.startsWith("text()")) {
                        // text(), "..."
                        test = test.substring(test.indexOf(",") + 1).strip(); // "..."
                        if (!(test.startsWith("\"") && test.endsWith("\""))) {
                            // String not surrounded by quotes. 
                            return false;
                        }
                        test = test.substring(1, test.length() - 1); // ...
                        if (event.getValue() != null && event.getValue().contains(test)) {
                            // Contains. 
                            return true;
                        }
                    }
                }
                return false;
                
            }
        }

        Deque<XSubPath> path = new LinkedList<>();
        Deque<XSubPath> xpath = new LinkedList<>();
        Deque<XSubPath> matchedXpath = new LinkedList<>();

        public MatchingStages(String exp) {
            boolean rootSkip = false;
            for (String s : exp.split("/")) {
                if (!rootSkip && s.length() == 0) {
                    rootSkip = true;
                    continue;
                }
                int testStart = s.indexOf("[");
                int testEnd = (testStart < 0) ? -1 : s.indexOf("]", testStart);
                if (testStart >= 0 && testEnd >= 0 && testEnd > testStart) {
                    // There is a test. 
                    this.xpath.addLast(new XSubPath(OccurrenceEvent.Type.Open, s.substring(0, testStart)));
                    this.xpath.addLast(new XSubPath(OccurrenceEvent.Type.Text, s.substring(testStart, testEnd + 1)));
                } else {
                    this.xpath.addLast(new XSubPath(OccurrenceEvent.Type.Open, s));
                }
            }
        }

        public boolean addStage(OccurrenceEvent event, boolean caseSensitivity) {
            if (event.getType() == OccurrenceEvent.Type.Open || event.getType() == OccurrenceEvent.Type.Text) {
                // ElementOpen or Text. 
                String curTag = event.getValue();
                path.add(new XSubPath(event.getType(), curTag));
                
                if (path.size() > matchedXpath.size() + 1) {
                    // Already unmatched. 
                    return false;
                }

                if (this.xpath.size() > 0 && this.xpath.getFirst().match(event, caseSensitivity)) {
                    this.matchedXpath.add(this.xpath.removeFirst());
                } else {
                    if (event.getType() == OccurrenceEvent.Type.Text) {
                        path.removeLast();
                    }
                }
            } else {
                // ElementClose. 
                while (path.size() > 0 && OccurrenceEvent.Type.Text.equals(path.removeLast().type)) {
                }
                if (path.size() < matchedXpath.size()) {
                    XSubPath last = matchedXpath.removeLast();
                    xpath.addFirst(last);
                    while (matchedXpath.size() > 0 && OccurrenceEvent.Type.Text.equals(last.type)) {
                        last = matchedXpath.removeLast();
                        xpath.addFirst(last);
                    }
                }
            }
            return this.xpath.size() == 0 && path.size() == matchedXpath.size();
        }
    }

    @Override
    public void setXPaths(String[] expressions) {
        this.expressions = expressions;
        this.xpaths = new MatchingStages[expressions.length];
        for (int i = 0; i < this.xpaths.length; i++) {
            this.xpaths[i] = new MatchingStages(expressions[i]);
        }
    }

    @Override
    public boolean[] evaluateEvent(OccurrenceEvent event, boolean caseSensitivity) {
        boolean[] matches = new boolean[this.xpaths.length];
        for (int i = 0; i < this.xpaths.length; i++) {
            matches[i] = this.xpaths[i].addStage(event, caseSensitivity);
        }
        return matches;
    }
    public boolean[] evaluateEvent(OccurrenceEvent event) {return evaluateEvent(event, false);}
    
}
