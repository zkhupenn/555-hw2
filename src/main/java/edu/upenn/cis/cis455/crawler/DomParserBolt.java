package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Parser;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DomParserBolt implements IRichBolt {
    private static Logger logger = LogManager.getLogger(LinkExtractorBolt.class);

    String executorId = UUID.randomUUID().toString();
    OutputCollector collector;
    Fields outFields = new Fields("url", "event", "contentType");

    @Override
    public String getExecutorId() {
		return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(this.outFields);
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void execute(Tuple input) {
        CrawlUrlQueueEntry entry = (CrawlUrlQueueEntry) input.getObjectByField("crawlerUrlQueueEntry");
        
        try {
            if (entry.conn != null && (entry.conn.getResponseCode() == 301 || entry.conn.getResponseCode() == 302)) {
                logger.info(entry.info.toString() + ": skipping redirect");
                return;
            }
        } catch (IOException e) {
            return;
        }

        parse(entry);
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return this.outFields;
    }



    void parse(CrawlUrlQueueEntry entry) {
        org.jsoup.nodes.Document doc;
        if (entry.contentType.contains("text/html")) {
            doc = Jsoup.parse(entry.body, entry.info.toString(), Parser.htmlParser().settings(ParseSettings.htmlDefault));
        } else if (entry.contentType.endsWith("xml")) {
            doc = Jsoup.parse(entry.body, entry.info.toString(), Parser.xmlParser().settings(ParseSettings.preserveCase));
        } else {
            return;
        }
        traverseNode(entry.info.toString(), doc, entry.contentType);
    }

    void traverseNode(String url, Node element, String contentType) {

        for (Node curE : element.childNodes()) {
            if (curE instanceof TextNode) {
                TextNode tn = (TextNode) curE;
                traverseText(url, tn, contentType);
            } else if (curE instanceof Element) {
                Element en = (Element) curE;
                // ElementOpen
                OccurrenceEvent openOcc = new OccurrenceEvent(OccurrenceEvent.Type.Open, en.tagName());
                this.collector.emit(new Values<Object>(url, openOcc, contentType));

                traverseNode(url, curE, contentType);

                // ElementClose
                OccurrenceEvent closeOcc = new OccurrenceEvent(OccurrenceEvent.Type.Close, en.tagName());
                this.collector.emit(new Values<Object>(url, closeOcc, contentType));
            } else {
                // Some other node. Skip. 
            }
        }


    }

    void traverseText(String url, TextNode textNode, String contentType) {
        if (textNode.isBlank()) {
            return;
        }

        // Text
        OccurrenceEvent textOcc = new OccurrenceEvent(OccurrenceEvent.Type.Text, textNode.text());
        this.collector.emit(new Values<Object>(url, textOcc, contentType));
    }
    
}