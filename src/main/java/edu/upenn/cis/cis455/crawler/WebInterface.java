package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static spark.Spark.*;

import edu.upenn.cis.cis455.crawler.handlers.HomeHandler;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.Document;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.crawler.utils.DocumentStorageFactory;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class WebInterface {
    public static void main(String args[]) {
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        port(45555);
        StorageInterface database = DocumentStorageFactory.getDocumentDatabaseInstance(args[0]);
        // database.addDocument("url", "documentContents");

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


        before("/*", "*/*", testIfLoggedIn);
        // /register, /logout, /index.html, /, /lookup
        post("/register", new RegistrationHandler(database));
        post("/login", new LoginHandler(database));
        get("/logout", new LogoutHandler(database));
        get("/index.html", new HomeHandler(database));
        get("/", new HomeHandler(database));
        get("/lookup", (req, res) -> {
            String urlQ = req.queryParams("url");
            if (urlQ == null) { halt(404); }
            String url = new URLInfo(urlQ).toString();
            Document doc = database.getDocument(url);
            if (doc == null) { halt(404); }
            return doc.getContent(); });

        get("/create/:name", (req, res) -> {
            // localhost:45555/create/{name}?xpath={xpath-pattern}

            String channelName = req.params(":name");
            String xpath = req.queryParams("xpath");
            if (channelName == null || xpath == null) {
                halt(404, "Not Found");
            }

            database.addChannel(channelName, new Channel(channelName, xpath, req.session().attribute("user")));
            return "Success";
        });

        get("/show", (req, res) -> {
            // localhost:45555/show?channel={name}

            String channelName = req.queryParams("channel");
            if (channelName == null) {
                halt(404, "Not Found");
            }

            Channel ch = database.getChannel(channelName);
            if (ch == null) {
                halt(404, "Not Found");
            }

            // Build channel html. 
            StringBuilder html = new StringBuilder();
            html.append("<html><head><title>" + ch.getName() + "</title></head><body>");
            html.append("<div class=\"channelheader\">");
            html.append("Channel name: " + ch.getName());
            html.append(", created by: " + ch.getCreatedByUser());
            html.append("</div>");

            for (String url : ch.getUrls()) {
                Document doc = database.getDocument(url);
                if (doc == null) {
                    continue;
                }

                html.append("<div>");

                html.append("Crawled on: " + doc.getCrawledOnTimeForChannel());
                html.append("Location: " + doc.getUrl());
                html.append("<div class=\"document\">" + doc.getContent() + "</div>");
                
                html.append("</div>");
            }

            html.append("</body></html>");
            
            return html.toString();
        });

        awaitInitialization();
    }
}
