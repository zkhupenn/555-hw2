package edu.upenn.cis.cis455.crawler;

import java.net.HttpURLConnection;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.Document;

public class CrawlUrlQueueEntry {
    boolean preped = false;
    public URLInfo info;
    
    public String lastModified = null;
    public String body = null;
    public String contentType = null;
    public Document existingDoc = null;
    public HttpURLConnection conn = null;

    public CrawlUrlQueueEntry(URLInfo info) {
        this.info = info;
    }
}
