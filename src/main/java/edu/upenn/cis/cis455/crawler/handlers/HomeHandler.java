package edu.upenn.cis.cis455.crawler.handlers;

import java.util.Map;

import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.HaltException;
import spark.Request;
import spark.Response;
import spark.Route;

public class HomeHandler implements Route {
    StorageInterface db;

    public HomeHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        StringBuilder html = new StringBuilder();
        Map<String, Channel> chMap = this.db.getAllChannels();
        String user = "";
        if (req.session(false) != null) {
            user = req.session(false).attribute("user");
        }
        
        html.append("<html><head><title>" + "Welcome " + user + "</title></head><body>");
        html.append("<div>");
        html.append("Welcome " + user);
        html.append("</div>");

        for (Channel ch : chMap.values()) {
            html.append("<div>");

            html.append(ch.getName());
            html.append("<a href=\"/show?channel=" + ch.getName() + "\">link</a>");
            
            html.append("</div>");
        }

        html.append("</body></html>");
        
        return html.toString();
    }
}