package edu.upenn.cis.cis455.crawler.utils;

import java.util.HashSet;

public class DocSet {
    HashSet<String> set = new HashSet<>();
    
    public boolean contains(String s) {
        synchronized(this.set) {
            return this.set.contains(s);
        }
    }

    public void add(String s) {
        synchronized(this.set) {
            this.set.add(s);
        }
    }

    public boolean containsAndAdd(String s) {
        synchronized(this.set) {
            boolean contains = this.set.contains(s);
            if (!contains) {
                this.set.add(s);
            }
            return contains;
        }
    }
}
