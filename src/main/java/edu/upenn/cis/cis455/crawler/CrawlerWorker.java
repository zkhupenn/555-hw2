package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.DocSet;
import edu.upenn.cis.cis455.crawler.utils.CrawlerHttpResponseContentGetter;
import edu.upenn.cis.cis455.crawler.utils.HttpURLConnectionFactory;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtChecker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.Document;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class CrawlerWorker implements Runnable {
    private static Logger logger = LogManager.getLogger(CrawlerWorker.class);

    Crawler crawler;
    CrawlerUrlQueue q;
    StorageInterface storage;
    long docMaxSize; // in bytes. 
    DocSet docSet;
    DocSet docHashSet;
    RobotsTxtChecker robotChecker;
    String userAgent;
    
    CrawlerWorker(Crawler c, CrawlerUrlQueue q, StorageInterface s, long maxMBSize, DocSet docSet, DocSet docHashSet, RobotsTxtChecker robotChecker, String userAgent) {
        this.crawler = c;
        this.q = q;
        this.storage = s;
        this.docMaxSize = ((long) maxMBSize) * 1024 * 1024;
        this.docSet = docSet;
        this.docHashSet = docHashSet;
        this.robotChecker = robotChecker;
        this.userAgent = userAgent;
    }

    @Override
    public void run() {
        logger.info("Crawler Worker Started. ");
        while (true) {
            this.crawler.setWorking(false);
            
            if (this.crawler.isDone()) {
                break;
            }

            CrawlUrlQueueEntry entry = this.q.pollAndSetWorking(100, this.crawler);
            if (entry == null) {
                continue;
            }
            // URLInfo info = entry.info;
            // this.crawler.setWorking(true);

            // logger.info(info.toString() + ": crawling");

            if (!entry.preped) {
                if (this.docSet.containsAndAdd(entry.info.toString())) {
                    logger.info(entry.info.toString() + ": skipping crawled");
                    continue;
                }
            }
            
            // Get new data. 
            // Check robot.txt. 
            Boolean canCrawl = this.robotChecker.canCrawl(entry.info);
            if (canCrawl != null && !canCrawl) {
                logger.info(entry.info.toString() + ": skipping cannot crawl");
                continue;
            }

            if (!entry.preped) {
                if (!prepEntryForGet(entry)) {
                    // No need to get. 
                    continue;
                }
            }

            // Get content. 
            if (entry.body == null) {
                
                if (canCrawl == null) {
                    // logger.info(entry.info.toString() + ": delaying crawl");
                    this.q.offer(entry);
                    continue;
                }

                if (!getContent(entry)) {
                    // No need to parse. 
                    continue;
                }
            }
            // logger.info("body: " + body);
            
            parse(entry);

        }

        this.crawler.notifyThreadExited();
    }

    boolean prepEntryForGet(CrawlUrlQueueEntry entry) {
        // First time deal with this crawl task. 
        entry.preped = true;

        // Get existing crawled doc. 
        entry.existingDoc = this.storage.getDocument(entry.info.toString());
        
        // Craw. 
        if (entry.existingDoc != null) {
            entry.lastModified = entry.existingDoc.getLastModified();
            entry.contentType = entry.existingDoc.getContentType();
            // body will be set when last modified is checked against HEAD response. 
        }

        try {
            // Check length and MIME type with HEAD. 
            HttpURLConnection metaConn = HttpURLConnectionFactory.head(entry.info);

            // logger.info(entry.info.toString() + ": HEAD");
            metaConn.setRequestProperty("User-Agent", this.userAgent);
            metaConn.connect();
            // logger.info(entry.info.toString() + ": HEAD " + metaConn.getResponseCode());

            if (metaConn.getResponseCode() == 200) {
                entry.contentType = metaConn.getHeaderField("content-type");
                if (entry.contentType != null && !(entry.contentType.contains("text/html") || entry.contentType.endsWith("xml"))) {
                    logger.info(entry.info.toString() + ": skipping HEAD metadata indicated non html or xml");
                    return false;
                }
                String contentLengthStr = metaConn.getHeaderField("content-length");
                if (contentLengthStr != null) {
                    int contentLength = Integer.parseInt(contentLengthStr);
                    if (!(contentLength < this.docMaxSize)) {
                        logger.info(entry.info.toString() + ": skipping HEAD metadata indicated too large");
                        return false;
                    }
                }
                

                String headLastModified = metaConn.getHeaderField("last-modified");
                if (entry.lastModified != null &&  headLastModified != null) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss zzz");
                    logger.info(entry.info.toString() + ": Head LM " + headLastModified + " LM " + entry.lastModified + " Change " + dateFormat.parse(headLastModified).after(dateFormat.parse(entry.lastModified)));
                    if (!dateFormat.parse(headLastModified).after(dateFormat.parse(entry.lastModified))) {
                        // Not modified since last crawl. 
                        // Set body to indicate no need to crawl. 
                        logger.info(entry.info.toString() + ": not modified");
                        entry.body = entry.existingDoc.getContent();
                    }
                }
            }
            
        } catch (ParseException e) {
        } catch (SocketTimeoutException e) {
            logger.info(entry.info.toString() + ": socket timeout");
        } catch (IOException e) {
        }
        return true;

    }

    boolean getContent(CrawlUrlQueueEntry entry) {

        HttpURLConnection conn = HttpURLConnectionFactory.get(entry.info);
        if (conn == null) {
            return false;
        }

        if (entry.existingDoc != null && entry.lastModified != null) {
            // Add if modified header. 
            conn.setRequestProperty("If-Modified-Since", entry.lastModified);
        }

        try {
            // logger.info(entry.info.toString() + ": GET");
            conn.setRequestProperty("User-Agent", this.userAgent);
            conn.connect();
            // logger.info(entry.info.toString() + ": GET " + conn.getResponseCode());

            if (conn.getResponseCode() == 301 || conn.getResponseCode() == 302) {
                // Redirect. 
                String docUrl = conn.getHeaderField("location");
                if (docUrl != null) {
                    this.q.offer(new CrawlUrlQueueEntry(new URLInfo(docUrl)));
                }
                logger.info(entry.info.toString() + ": skipping redirect");
                return false;
            } else if (conn.getResponseCode() == 304) {
                entry.body = entry.existingDoc.getContent();
                logger.info(entry.info.toString() + ": not modified");
            } else if (conn.getResponseCode() == 200) {
                entry.body = CrawlerHttpResponseContentGetter.getContent(conn, entry.info.isSecure());
                logger.info(entry.info.toString() + ": downloading");
            } else {
                logger.info(entry.info.toString() + ": " + conn.getResponseCode() + " skipping failed request");
                return false;
            }
        } catch (SocketTimeoutException e) {
            logger.info(entry.info.toString() + ": socket timeout");
        } catch (IOException e) {
            return false;
        }
        
        // Parse body. 
        if (entry.body == null || entry.body.length() <= 0) {
            logger.info(entry.info.toString() + ": skipping no body");
            return false;
        }

        // Check validity again. 
        if (conn.getContentType() == null || !(conn.getContentType().contains("text/html") || conn.getContentType().endsWith("xml"))) {
            logger.info(entry.info.toString() + ": skipping no content type or not html or xml");
            return false;
        }
        if (conn.getContentLength() <= 0 || conn.getContentLength() > this.docMaxSize) {
            logger.info(entry.info.toString() + ": skipping no content length or too large");
            return false;
        }

        // Update last modified. 
        if (conn.getContentType() != null) {
            entry.contentType = conn.getContentType();
        }
        if (conn.getHeaderField("last-modified") != null) {
            entry.lastModified = conn.getHeaderField("last-modified");
        }

        return true;
    }

    void parse(CrawlUrlQueueEntry entry) {
        // Check if md5 same. 
        String md5Hash = edu.upenn.cis.cis455.storage.Document.md5Hash(entry.body);
        if (!this.docHashSet.containsAndAdd(md5Hash)) {
            // MD5 not found. 

            this.storage.addDocument(entry.info.toString(), new Document(entry.info.toString(), entry.body, entry.lastModified, entry.contentType, System.currentTimeMillis()));
            this.crawler.incCount();
            logger.info(entry.info.toString() + ": crawed");

            if (entry.contentType == null || entry.contentType.contains("text/html")) {
                // Parse document for additional urls. 
                org.jsoup.nodes.Document doc = Jsoup.parse(entry.body, entry.info.toString());
                //Get links. 
                Elements links = doc.select("[href]");
            
                //Iterate links and print link attributes.
                for (Element e : links) {
                    String link = e.attr("abs:href");
                    logger.info(link + ": queued");
                    this.q.offer(new CrawlUrlQueueEntry(new URLInfo(link)));
                }
            }
        }
    }
    
}
