package edu.upenn.cis.cis455.crawler.utils;

import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class DocumentStorageFactory {
    private static Map<String, StorageInterface> storages = new HashMap<>();
    public static StorageInterface getDocumentDatabaseInstance(String dir) {
        if (!storages.containsKey(dir)) {
            storages.put(dir, StorageFactory.getDatabaseInstance(dir));
        }
        return storages.get(dir);
    }
}
