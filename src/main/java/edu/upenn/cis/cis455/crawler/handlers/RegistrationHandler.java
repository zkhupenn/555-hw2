package edu.upenn.cis.cis455.crawler.handlers;

import static spark.Spark.*;
import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegistrationHandler implements Route {
    StorageInterface db;

    public RegistrationHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Registration request for " + user);
        String username = db.addUser(user, pass);
        if (username != null) {
            System.err.println("Registration Successful! ");
            Session session = req.session();

            session.attribute("user", user);
            session.attribute("password", pass);
            return "Registration Successful! <a href='/login-form.html'></a>";
        } else {
            System.err.println("User Existed. ");
            halt(409, "User Existed. <a href='/login-form.html'></a>");
            return "User Existed. <a href='/login-form.html'></a>";
        }
    }
}
