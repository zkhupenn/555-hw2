package edu.upenn.cis.cis455.crawler.utils;

import java.util.HashMap;
import java.util.Map;

public class RobotsTxtCheckerFactory {
    private static Map<String, RobotsTxtChecker> checkers = new HashMap<>();
    public static RobotsTxtChecker getRobotTxtChecker(String userAgent) {
        if (!checkers.containsKey(userAgent)) {
            checkers.put(userAgent, new RobotsTxtChecker(userAgent));
        }
        return checkers.get(userAgent);
    }
}
