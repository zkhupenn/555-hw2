package edu.upenn.cis.cis455.crawler.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RobotsTxtChecker {

    class RobotsTxtGroup {
        Set<String> allowSet = new HashSet<>();
        Set<String> disallowSet = new HashSet<>();
        int crawlDelay = 0;
        long lastCrawlTime = 0;
    }

    String userAgent;

    Map<String, RobotsTxtGroup> map = new HashMap<>();

    Set<String> visited = new HashSet<>();

    public RobotsTxtChecker(String userAgent) {
        this.userAgent = userAgent;
    }

    /**
     * Check if url can be crawled. 
     * @param info
     * @return Boolean object. True if can crawl immediately, false if cannot crawl anytime, null if cannot crawl now due to crawl delay. 
     */
    public Boolean canCrawl(URLInfo info) {
        synchronized(this) {
            String id = (info.isSecure() ? "https://" : "http://") + info.getHostName() + ":" + info.getPortNo();
            if (!visited.contains(id)) {
                // Fetch robot.txt and parse. 
                visited.add(id);

                // Fetch robot.txt. 
                URLInfo robotInfo = new URLInfo(info.getHostName(), info.getPortNo(), "/robots.txt");
                robotInfo.setSecure(info.isSecure());
                HttpURLConnection conn = HttpURLConnectionFactory.get(robotInfo);
                String body = null;
                try {
                    conn.setRequestProperty("User-Agent", this.userAgent);
                    conn.connect();
                    body = CrawlerHttpResponseContentGetter.getContent(conn, info.isSecure());
                } catch (IOException e) {
                    return true;
                }
                if (body == null || body.length() == 0) {
                    return true;
                }
                
                // Parse body. 
                String[] lines = body.trim().split("\n");
                boolean curUserAgentDef = false;
                boolean userAgentMatch = false;

                RobotsTxtGroup untightMatchGroup = null;
                RobotsTxtGroup curGroup = new RobotsTxtGroup();
                boolean isTightMatch = false;

                for (String line : lines) {
                    String[] entries = line.trim().split(":");
                    if (entries.length < 2) {
                        continue;
                    }
                    
                    String key = entries[0].trim().toLowerCase();
                    String value = entries[1].trim();

                    if (key.equals("user-agent")) {
                        if (!curUserAgentDef) {
                            // New User-agent section. Reset rules application. 
                            if (userAgentMatch && isTightMatch) {
                                break;
                            } else if (userAgentMatch && !isTightMatch) {
                                untightMatchGroup = curGroup;
                                curGroup = new RobotsTxtGroup();
                                userAgentMatch = false;
                            }
                        }
                        curUserAgentDef = true;
                        
                        if ("*".equals(value)) {
                            userAgentMatch = true;
                        }
                        if (this.userAgent.equals(value)) {
                            userAgentMatch = true;
                            isTightMatch = true;
                        }
                    } else {
                        curUserAgentDef = false;

                        if (userAgentMatch) {
                            if ("allow".equals(key)) {
                                curGroup.allowSet.add(value);
                            } else if ("disallow".equals(key)) {
                                curGroup.disallowSet.add(value);
                            } else if ("crawl-delay".equals(key)) {
                                curGroup.crawlDelay = Integer.parseInt(value);
                            }
                        }
                    }
                }
                
                if (userAgentMatch) {
                    // Either last group in robot.txt or the closest matching group. 
                    // Use curGroup. 
                    this.map.put(id, curGroup);
                } else if (!userAgentMatch && untightMatchGroup != null) {
                    // Previously seen untight matching group and no tight matching group. 
                    this.map.put(id, untightMatchGroup);
                }
            }

            // Now have the group. 
            if (!this.map.containsKey(id)) {
                return true;
            }

            RobotsTxtGroup rules = this.map.get(id);
            String allowRule = getMatchingRule(rules.allowSet, info.getFilePath());
            String disallowRule = getMatchingRule(rules.disallowSet, info.getFilePath());
            boolean allow = true;
            if (allowRule != null && disallowRule != null) {
                allow = allowRule.length() >= disallowRule.length();
            } else if (allowRule != null) {
                allow = true;
            } else if (disallowRule != null) {
                allow = false;
            } else {
                allow = true;
            }

            // Return false if disallowed. 
            if (!allow) {
                return false;
            }

            // Return true if allowed and no crawl-delay. 
            if (rules.crawlDelay <= 0) {
                return true;
            }

            // Check crawl-delay. 
            long curTime = new Date().getTime();
            if (rules.lastCrawlTime + rules.crawlDelay * 1000 <= curTime) {
                rules.lastCrawlTime = curTime;
                return true;
            } else {
                return null;
            }
        }
    }

    String getMatchingRule(Set<String> set, String path) {
        String longestMatching = null;
        for (String r : set) {
            if (longestMatching != null && longestMatching.length() > r.length()) {
                continue;
            }

            String[] words = r.split("\\*");
            boolean matched = true;
            for (int i = 0; i < words.length; i++) {
                String w = words[i];

                if (i == 0) {
                    if (w.endsWith("$")) {
                        // Match from behind. 
                        if (path.equals(w.substring(0, w.length() - 1))) {
                            path = "";
                        } else {
                            matched = false;
                            break;
                        }
                    } else {
                        // Match from ahead. 
                        if (path.startsWith(w)) {
                            path = path.substring(w.length());
                        } else {
                            matched = false;
                            break;
                        }
                    }
                } else {
                    if (w.endsWith("$")) {
                        // Match from behind. 
                        if (path.endsWith(w.substring(0, w.length() - 1))) {
                            path = path.substring(0, path.length() - w.length() + 1);
                        } else {
                            matched = false;
                            break;
                        }
                    } else {
                        // Match something. 
                        int idx = path.indexOf(w);
                        if (idx != -1) {
                            path = path.substring(idx + w.length());
                        } else {
                            matched = false;
                            break;
                        }
                    }
                }
            }

            if (matched) {
                longestMatching = r;
            }
            
        }
        return longestMatching;
    }
}
