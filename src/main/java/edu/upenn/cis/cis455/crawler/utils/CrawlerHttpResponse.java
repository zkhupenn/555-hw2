package edu.upenn.cis.cis455.crawler.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

public class CrawlerHttpResponse extends HttpURLConnection {
    
    URLInfo info;
    HashMap<String, String> reqHeaders = new HashMap<>();
    
    HashMap<String, String> respHeaders = new HashMap<>();
    int respCode = -1;
    String body = "";
    String method = "GET";

    protected CrawlerHttpResponse(URL u) {
        super(u);
    }

    public CrawlerHttpResponse(URLInfo info) throws IOException {
        super(null);

        this.info = info;
    }

    @Override
    public void disconnect() {
    }

    @Override
    public boolean usingProxy() {
        return false;
    }

    @Override
    public void connect() throws SocketTimeoutException, IOException {
        // HTTP. 
        // Request. 
        Socket s;
        
        s = new Socket();
        s.connect(new InetSocketAddress(info.getHostName(), info.getPortNo()), 10000);
        s.setSoTimeout(10000);

        DataOutputStream out = new DataOutputStream(s.getOutputStream());
        out.write((
            this.method + " " + info.getFilePath() + " HTTP/1.1\r\n" + 
            "Host: " + info.getHostName() + "\r\n"
        ).getBytes());
        for (Entry<String, String> e : this.reqHeaders.entrySet()) {
            out.write((e.getKey() + ": " + e.getValue() + "\r\n").getBytes());
        }
        out.write("\r\n".getBytes());
        out.flush();

        // Read response. 
        BufferedReader inReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        String outStr = inReader.readLine();
        if (outStr == null) {
            s.close();
            throw new IOException();
        }

        String[] header = outStr.split(" ");
        this.respCode = Integer.parseInt(header[1]);

        while((outStr = inReader.readLine()) != null){
            if (outStr.length() == 0) {
                break;
            }
            int splitter = outStr.indexOf(":");
            respHeaders.put(outStr.substring(0, splitter).trim().toLowerCase(), outStr.substring(splitter + 1).trim());
        }

        if (this.respCode == 200) {
            // 200 OK. 
            if (!respHeaders.containsKey("content-length")) {
                this.body = "";
                s.close();
                return;
            }
            int contentLength = Integer.parseInt(this.getHeaderField("content-length"));
            // String contentType = this.getHeaderField("content-type");
            
            if (contentLength > 0) {
                StringBuilder bodyBuilder = new StringBuilder();
                char[] buffer = new char[512];

                int numRead = 0;
                int totalRead = 0;

                while (totalRead < contentLength && numRead != -1) {
                    numRead = inReader.read(buffer);
                    if (numRead > 0) {
                        bodyBuilder.append(buffer, 0, numRead);
                        totalRead += numRead;
                    }
                }
                this.body = bodyBuilder.toString();
            }
        }
        
        // Close socket. 
        s.close();
    }

    @Override
    public int getResponseCode() throws IOException {
        return this.respCode;
    }

    @Override
    public String getHeaderField(String name) {
        return this.respHeaders.get(name.toLowerCase());
    }

    @Override
    public Object getContent() throws IOException {
        return this.body;
    }

    @Override
    public String getContentType() {
        return respHeaders.get("content-type");
    }

    @Override
    public int getContentLength() {
        return this.body.length();
    }

    @Override
    public long getContentLengthLong() {
        return this.getContentLength();
    }

    @Override
    public void setRequestProperty(String key, String value) {
        reqHeaders.put(key, value);
    }
    
    @Override
    public void setRequestMethod(String method) throws ProtocolException {
        this.method = method;
    }
}
