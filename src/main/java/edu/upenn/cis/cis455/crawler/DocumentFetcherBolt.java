package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.CrawlerHttpResponseContentGetter;
import edu.upenn.cis.cis455.crawler.utils.DocSet;
import edu.upenn.cis.cis455.crawler.utils.DocumentStorageFactory;
import edu.upenn.cis.cis455.crawler.utils.HashDocSetFactory;
import edu.upenn.cis.cis455.crawler.utils.HttpURLConnectionFactory;
import edu.upenn.cis.cis455.storage.Document;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocumentFetcherBolt implements IRichBolt {
    private static Logger logger = LogManager.getLogger(DocumentFetcherBolt.class);

    String executorId = UUID.randomUUID().toString();
    OutputCollector collector;
    Fields outFields = new Fields("crawlerUrlQueueEntry");
    
    String userAgent = null;
    int docMaxMBSize = -1;

    DocSet docHashSet = HashDocSetFactory.getHashDocSet();
    StorageInterface storage;


    @Override
    public String getExecutorId() {
		return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(this.outFields);
    }

    @Override
    public void cleanup() {
        
    }

    @Override
    public void execute(Tuple input) {
        CrawlUrlQueueEntry entry = (CrawlUrlQueueEntry) input.getObjectByField("crawlerUrlQueueEntry");
        logger.info("Fetching: " + entry.info.toString());
        
        if (!getContent(entry)) {
            // No need to continue to parse. 
            return;
        }
        // Has a body or is a redirect. 
        // Continue to parse and extract links. 
        this.collector.emit(new Values<Object>(entry));
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.userAgent = stormConf.getOrDefault("userAgent", null);
        this.docMaxMBSize = Integer.parseInt(stormConf.getOrDefault("docMaxSize", "-1"));

        this.storage = DocumentStorageFactory.getDocumentDatabaseInstance(stormConf.getOrDefault("docDir", "./"));
    }

    @Override
    public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return this.outFields;
    }
    

    
    boolean getContent(CrawlUrlQueueEntry entry) {
        // Get content. 
        if (entry.body != null) {
            return true;
        }

        HttpURLConnection conn = HttpURLConnectionFactory.get(entry.info);
        if (conn == null) {
            return false;
        }
        entry.conn = conn;

        if (entry.existingDoc != null && entry.lastModified != null) {
            // Add if modified header. 
            conn.setRequestProperty("If-Modified-Since", entry.lastModified);
        }

        try {
            // logger.info(entry.info.toString() + ": GET");
            if (this.userAgent != null) {
                conn.setRequestProperty("User-Agent", this.userAgent);
            }
            conn.connect();
            // logger.info(entry.info.toString() + ": GET " + conn.getResponseCode());

            if (conn.getResponseCode() == 301 || conn.getResponseCode() == 302) {
                // Redirect. 
                logger.info(entry.info.toString() + ": skipping redirect");
                return true;
            } else if (conn.getResponseCode() == 304) {
                entry.body = entry.existingDoc.getContent();
                logger.info(entry.info.toString() + ": not modified");
            } else if (conn.getResponseCode() == 200) {
                entry.body = CrawlerHttpResponseContentGetter.getContent(conn, entry.info.isSecure());
                logger.info(entry.info.toString() + ": downloading");
            } else {
                logger.info(entry.info.toString() + ": " + conn.getResponseCode() + " " + conn.getResponseMessage() + " skipping failed request");
                return false;
            }
        } catch (SocketTimeoutException e) {
            logger.info(entry.info.toString() + ": socket timeout");
        } catch (IOException e) {
            return false;
        }
        
        // Parse body. 
        if (entry.body == null || entry.body.length() <= 0) {
            logger.info(entry.info.toString() + ": skipping no body");
            return false;
        }

        // Check validity again. 
        if (conn.getContentType() == null || !(conn.getContentType().contains("text/html") || conn.getContentType().endsWith("xml"))) {
            logger.info(entry.info.toString() + ": skipping no content type or not html or xml");
            return false;
        }
        if (conn.getContentLength() <= 0 || (this.docMaxMBSize >= 0 && conn.getContentLength() > this.docMaxMBSize * 1024 * 1024)) {
            logger.info(entry.info.toString() + ": skipping no content length or too large");
            return false;
        }

        // Update last modified. 
        if (conn.getContentType() != null) {
            entry.contentType = conn.getContentType();
        }
        if (conn.getHeaderField("last-modified") != null) {
            entry.lastModified = conn.getHeaderField("last-modified");
        }

        // Check if md5 existed. 
        String md5Hash = edu.upenn.cis.cis455.storage.Document.md5Hash(entry.body);
        if (this.docHashSet.containsAndAdd(md5Hash)) {
            // MD5 found. 
            return false;
        }

        this.storage.addDocument(entry.info.toString(), new Document(entry.info.toString(), entry.body, entry.lastModified, entry.contentType, System.currentTimeMillis()));
        // this.crawler.incCount();
        logger.info(entry.info.toString() + ": crawed");

        return true;
    }
}
