package edu.upenn.cis.cis455.crawler;

import java.util.LinkedList;
import java.util.Queue;

public class CrawlerUrlQueue {
    
    Queue<CrawlUrlQueueEntry> q = new LinkedList<>();

    public void offer(CrawlUrlQueueEntry u) {
        synchronized(q) {
            q.offer(u);
            q.notify();
        }
    }

    public CrawlUrlQueueEntry pollAndSetWorking(int waitMs, Crawler crawler) {
        synchronized(q) {
            if (q.size() == 0) {
                try {
                    q.wait(waitMs);
                } catch (InterruptedException e) {
                    return null;
                }
            }
            
            CrawlUrlQueueEntry info = q.poll();
            if (info != null) {
                crawler.setWorking(true);
                return info;
            } else {
                return null;
            }
        }
    }

    public CrawlUrlQueueEntry poll() {
        synchronized(q) {
            return q.poll();
        }
    }

    public int size() {
        synchronized(q) {
            return q.size();
        }
    }
    
}
