package edu.upenn.cis.cis455.crawler.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionFactory {

    public static HttpURLConnection get(URLInfo info) {
        HttpURLConnection conn = null;
        try {
            if (!info.isSecure()) {
                // HTTP. 
                conn = new CrawlerHttpResponse(info);
                conn.setRequestMethod("GET");
            } else {
                // HTTPS.
                conn = (HttpURLConnection) new URL(info.toString()).openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
            }
        } catch (IOException e) {
            return null;
        }
        return conn;
    }
    public static HttpURLConnection head(URLInfo info) {
        HttpURLConnection conn = null;
        try {
            if (!info.isSecure()) {
                // HTTP. 
                conn = new CrawlerHttpResponse(info);
                conn.setRequestMethod("HEAD");
            } else {
                // HTTPS.
                conn = (HttpURLConnection) new URL(info.toString()).openConnection();
                conn.setRequestMethod("HEAD");
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
            }
        } catch (IOException e) {
            return null;
        }
        return conn;

    }
    
}
