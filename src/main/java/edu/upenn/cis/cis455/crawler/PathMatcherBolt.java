package edu.upenn.cis.cis455.crawler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.DocumentStorageFactory;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PathMatcherBolt implements IRichBolt {
    private static Logger logger = LogManager.getLogger(PathMatcherBolt.class);

    String executorId = UUID.randomUUID().toString();
    OutputCollector collector;
    Fields outFields = new Fields("TODO");

    Map<String, XPathEngine> engines = new HashMap<>();
    Map<String, Integer> enginesState = new HashMap<>();
    StorageInterface storage;
    String[] channelNames;
    String[] channelXpaths;

    @Override
    public String getExecutorId() {
		return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(this.outFields);
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void execute(Tuple input) {
        String url = input.getStringByField("url");
        OccurrenceEvent occ = (OccurrenceEvent) input.getObjectByField("event");
        String contentType = input.getStringByField("contentType");
        // logger.info(occ.getType() + " " + occ.getValue());

        // Get the XPathEngine for this url. 
        if (!this.engines.containsKey(url)) {
            XPathEngine newEng = XPathEngineFactory.getXPathEngine();
            newEng.setXPaths(this.channelXpaths);
            this.engines.put(url, newEng);
        }
        XPathEngine eng = this.engines.get(url);

        // Check if current event matches any XPath. 
        boolean caseSensitivity = true;
        if (contentType.contains("text/html")) {
            caseSensitivity = false;
        } else if (contentType.endsWith("xml")) {
            caseSensitivity = true;
        }
        boolean[] matches;
        try {
            matches = eng.evaluateEvent(occ, caseSensitivity);
        } catch (Exception e) {
            return;
        }
        for (int i = 0; i < matches.length; i++) {
            if (matches[i]) {
                logger.info("Match: " + this.channelNames[i] + " " + url);
                // Add url to channel for each match. 
                Channel ch = this.storage.getChannel(this.channelNames[i]);
                ch.addDoc(url);
                // Save channel into database. 
                this.storage.addChannel(ch.getName(), ch);
            }
        }

        // Keep track of whether the document has finished parsing. 
        if (OccurrenceEvent.Type.Open.equals(occ.getType())) {
            this.enginesState.put(url, this.enginesState.getOrDefault(url, 0) + 1);
        } else if (OccurrenceEvent.Type.Close.equals(occ.getType())) {
            this.enginesState.put(url, this.enginesState.getOrDefault(url, 0) - 1);
        }
        if (this.enginesState.getOrDefault(url, 0) <= 0) {
            this.engines.remove(url);
            this.enginesState.remove(url);
        }
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        
        this.storage = DocumentStorageFactory.getDocumentDatabaseInstance(stormConf.getOrDefault("docDir", "./"));

        Map<String, Channel> channels = this.storage.getAllChannels();
        this.channelNames = new String[channels.size()];
        this.channelXpaths = new String[channels.size()];
        int i = 0;
        for (Channel ch : channels.values()) {
            this.channelNames[i] = ch.getName();
            this.channelXpaths[i] = ch.getXpath();
            i++;
        }
    }

    @Override
    public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return this.outFields;
    }
    
}
