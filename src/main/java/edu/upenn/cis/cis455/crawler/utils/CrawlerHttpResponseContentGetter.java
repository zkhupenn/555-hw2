package edu.upenn.cis.cis455.crawler.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CrawlerHttpResponseContentGetter {
    private static Logger logger = LogManager.getLogger(CrawlerHttpResponseContentGetter.class);

    public static String getContent(HttpURLConnection conn, boolean isSecure) throws SocketTimeoutException, IOException {
        if (!isSecure) {
            // HTTP is CrawlerHttpResponse. Use getContent(). 
            return (String) conn.getContent();
        }
        logger.info("start retrieving all content");

        // HTTPS needs to read from input stream. 
        BufferedReader inReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        
        int contentLength = conn.getContentLength();
        
        StringBuilder bodyBuilder = new StringBuilder();
        char[] buffer = new char[512];

        int numRead = 0;
        int totalRead = 0;

        while (totalRead < contentLength && numRead != -1) {
            numRead = inReader.read(buffer);
            if (numRead > 0) {
                bodyBuilder.append(buffer, 0, numRead);
                totalRead += numRead;
            }
        }
        logger.info("retrieved all content");
        return bodyBuilder.toString();
    }
}
