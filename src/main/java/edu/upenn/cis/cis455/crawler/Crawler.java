package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.DocSet;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtChecker;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;

public class Crawler implements CrawlMaster {
    private static Logger logger = LogManager.getLogger(Crawler.class);
    // You'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.

    public String userAgent = "cis455crawler";

    private StorageInterface db;
    private int maxSize;
    private int workerPoolSize = 1;
    private int docMaxCount;

    private Integer count = 0;
    private boolean isDone = false;
    private Integer quittedWorkerCount = 0;

    private CrawlerUrlQueue urlQueue = new CrawlerUrlQueue();
    private HashMap<Thread, Boolean> workerWorking = new HashMap<>();
    private DocSet docSet = new DocSet();
    private DocSet docHashSet = new DocSet();
    private RobotsTxtChecker robotChecker;

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        synchronized(this) {
            this.urlQueue.offer(new CrawlUrlQueueEntry(new URLInfo(startUrl)));
        }
        this.db = db;
        this.maxSize = size;
        this.docMaxCount = count;
    }

    /**
     * Main thread
     */
    public void start() {
        robotChecker = new RobotsTxtChecker(this.userAgent);
        for (int i = 0; i < this.workerPoolSize; i++) {
            Thread t = new Thread(new CrawlerWorker(this, this.urlQueue, this.db, this.maxSize, this.docSet, this.docHashSet, this.robotChecker, this.userAgent));
            t.start();
            workerWorking.put(t, false);
        }
    }

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
        synchronized(this.count) {
            this.count += 1;
        }
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
        int count;
        synchronized(this.count) {
            count = this.count;
        }
        boolean workerRunning;
        synchronized(this.workerWorking) {
            workerRunning = this.workerWorking.containsValue(true);
        }
        return (this.urlQueue.size() == 0 && !workerRunning) || (count >= this.docMaxCount) || (this.isDone);
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
        synchronized(this.workerWorking) {
            workerWorking.put(Thread.currentThread(), working);
        }
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
        synchronized(this.quittedWorkerCount) {
            this.quittedWorkerCount++;
            synchronized(this.workerWorking) {
                workerWorking.put(Thread.currentThread(), false);
            }

            // Shutdown. 
            if (this.quittedWorkerCount >= this.workerPoolSize) {
                // Shutdown. 
                // this.quittedWorkerCount.notifyAll();
            }
        }
    }

    public void shutdown() {
        this.isDone = true;
        synchronized(this.quittedWorkerCount) {
            while (this.quittedWorkerCount < this.workerPoolSize) {
                try {
                    this.quittedWorkerCount.wait(100);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;
        

        if (!Files.exists(Paths.get(envPath))) {
            try {
                Files.createDirectory(Paths.get(envPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
        // db.addChannel("pages", new Channel("pages", "/html/head/title", "apple"));
        // db.addChannel("login", new Channel("login", "/html/head/title[text()  =     \"Log in\"   ]", "banana"));
        // db.addChannel("nonologin", new Channel("nonologin", "/html/head/title[text()  =     \"log in\"   ]", "banana"));
        // db.addChannel("signup", new Channel("signup", "/html/head/title[  contains(  text() ,   \"egist\" )  ]", "peach"));

        // Crawler crawler = new Crawler(startUrl, db, size, count);

        // System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        // crawler.start();

        // while (!crawler.isDone()) {
        //     try {
        //         Thread.sleep(1000);
        //         logger.info("Crawled: " + crawler.count + " Quitted: " + crawler.quittedWorkerCount + " Left: " + crawler.urlQueue.size() + " Working: " + crawler.workerWorking.containsValue(true));
        //     } catch (InterruptedException e) {
        //         e.printStackTrace();
        //     }
        // }

        // // Final shutdown. 
        // crawler.shutdown();
        // db.close();

        // System.out.println("Done crawling!");






        // Use StormLite. 

        
        Config config = new Config();
        config.put("startUrl", startUrl);
        config.put("userAgent", "cis455crawler");
        config.put("docMaxSize", size.toString());
        config.put("docDir", envPath);
        config.put("maxCount", count.toString());

        CrawlerQueueSpout queueSpout = new CrawlerQueueSpout();
        DocumentFetcherBolt docFetch = new DocumentFetcherBolt();
        LinkExtractorBolt linkEx = new LinkExtractorBolt();
        DomParserBolt domParser = new DomParserBolt();
        PathMatcherBolt pathMatcher = new PathMatcherBolt();

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("url", queueSpout, 4);
        builder.setBolt("docs", docFetch, 4).shuffleGrouping("url");
        builder.setBolt("links", linkEx, 4).shuffleGrouping("docs");
        builder.setBolt("dom", domParser, 4).shuffleGrouping("docs");
        builder.setBolt("path", pathMatcher, 4).fieldsGrouping("dom", new Fields("url"));

        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        
        
        cluster.submitTopology("test", config, builder.createTopology());
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
        }
        // System.out.println(db.getChannel("pages"));
        // System.out.println(db.getChannel("login"));
        // System.out.println(db.getChannel("signup"));
        cluster.killTopology("test");
        cluster.shutdown();
    }

}
