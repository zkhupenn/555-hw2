package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.DocSet;
import edu.upenn.cis.cis455.crawler.utils.DocumentStorageFactory;
import edu.upenn.cis.cis455.crawler.utils.HttpURLConnectionFactory;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtChecker;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtCheckerFactory;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.crawler.utils.UrlDocSetFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class CrawlerQueueSpout implements IRichSpout {
    private static Logger logger = LogManager.getLogger(CrawlerQueueSpout.class);
    
	SpoutOutputCollector collector;
    String executorId = UUID.randomUUID().toString();
    Fields outFields = new Fields("crawlerUrlQueueEntry");
    
    CrawlerUrlQueue q = CrawlerUrlQueueFactory.getFrontier();
    DocSet urlDocSet = UrlDocSetFactory.getUrlDocSet();
    RobotsTxtChecker robotChecker;
    StorageInterface storage;

    String userAgent = null;
    int docMaxMBSize = -1;

    @Override
    public String getExecutorId() {
		return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(this.outFields);
    }

    @Override
    public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
        this.collector = collector;
        this.userAgent = config.getOrDefault("userAgent", null);
        this.docMaxMBSize = Integer.parseInt(config.getOrDefault("docMaxSize", "-1"));

        this.robotChecker = RobotsTxtCheckerFactory.getRobotTxtChecker(this.userAgent);
        this.storage = DocumentStorageFactory.getDocumentDatabaseInstance(config.getOrDefault("docDir", "./"));

        this.q.offer(new CrawlUrlQueueEntry(new URLInfo(config.getOrDefault("startUrl", ""))));
    }

    @Override
    public void close() {
    }

    @Override
    public void nextTuple() {
        CrawlUrlQueueEntry entry = q.poll();
        if (entry == null) {
            return;
        }

        if (!entry.preped) {
            if (this.urlDocSet.containsAndAdd(entry.info.toString())) {
                logger.info(entry.info.toString() + ": skipping crawled");
                return;
            }
        }
        
        // Get new data. 
        // Check robot.txt. 
        Boolean canCrawl = this.robotChecker.canCrawl(entry.info);
        if (canCrawl != null && !canCrawl) {
            logger.info(entry.info.toString() + ": skipping cannot crawl");
            return;
        }

        if (!entry.preped) {
            if (!prepEntryForGet(entry)) {
                // Content type not matched or too large. 
                return;
            }
        }

        // Get content. 
        if (entry.body == null) {
            if (canCrawl == null) {
                // logger.info(entry.info.toString() + ": delaying crawl");
                this.q.offer(entry);
                return;
            }
        }
        this.collector.emit(new Values<Object>(entry));
    }

    @Override
    public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
    }

    
    
    boolean prepEntryForGet(CrawlUrlQueueEntry entry) {
        // First time deal with this crawl task. 
        entry.preped = true;

        // Get existing crawled doc. 
        entry.existingDoc = this.storage.getDocument(entry.info.toString());
        
        // Craw. 
        if (entry.existingDoc != null) {
            entry.lastModified = entry.existingDoc.getLastModified();
            entry.contentType = entry.existingDoc.getContentType();
            // body will be set when last modified is checked against HEAD response. 
        }

        try {
            // Check length and MIME type with HEAD. 
            HttpURLConnection metaConn = HttpURLConnectionFactory.head(entry.info);

            // logger.info(entry.info.toString() + ": HEAD");
            if (this.userAgent != null) {
                metaConn.setRequestProperty("User-Agent", this.userAgent);
            }
            metaConn.connect();
            // logger.info(entry.info.toString() + ": HEAD " + metaConn.getResponseCode());

            if (metaConn.getResponseCode() == 200) {
                entry.contentType = metaConn.getHeaderField("content-type");
                if (entry.contentType != null && !(entry.contentType.contains("text/html") || entry.contentType.endsWith("xml"))) {
                    logger.info(entry.info.toString() + ": skipping HEAD metadata indicated non html or xml");
                    return false;
                }
                String contentLengthStr = metaConn.getHeaderField("content-length");
                if (contentLengthStr != null) {
                    int contentLength = Integer.parseInt(contentLengthStr);
                    if ((contentLength >= this.docMaxMBSize * 1024 * 1024) && (this.docMaxMBSize != -1)) {
                        logger.info(entry.info.toString() + ": skipping HEAD metadata indicated too large " + contentLength);
                        return false;
                    }
                }
                

                String headLastModified = metaConn.getHeaderField("last-modified");
                if (entry.lastModified != null &&  headLastModified != null) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss zzz");
                    logger.info(entry.info.toString() + ": Head LM " + headLastModified + " LM " + entry.lastModified + " Change " + dateFormat.parse(headLastModified).after(dateFormat.parse(entry.lastModified)));
                    if (!dateFormat.parse(headLastModified).after(dateFormat.parse(entry.lastModified))) {
                        // Not modified since last crawl. 
                        // Set body to indicate no need to crawl. 
                        logger.info(entry.info.toString() + ": not modified");
                        entry.body = entry.existingDoc.getContent();
                    }
                }
            }
            
        } catch (ParseException e) {
        } catch (SocketTimeoutException e) {
            logger.info(entry.info.toString() + ": socket timeout");
        } catch (IOException e) {
        }
        return true;

    }
}
