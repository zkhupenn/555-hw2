package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class LinkExtractorBolt implements IRichBolt {
    private static Logger logger = LogManager.getLogger(LinkExtractorBolt.class);

    String executorId = UUID.randomUUID().toString();
    OutputCollector collector;
    Fields outFields = new Fields("crawlerUrlQueueEntry");

    CrawlerUrlQueue q = CrawlerUrlQueueFactory.getFrontier();

    @Override
    public String getExecutorId() {
		return executorId;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(this.outFields);
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void execute(Tuple input) {
        CrawlUrlQueueEntry entry = (CrawlUrlQueueEntry) input.getObjectByField("crawlerUrlQueueEntry");
        logger.info("Extracting: " + entry.info.toString());
        
        try {
            if (entry.conn != null && (entry.conn.getResponseCode() == 301 || entry.conn.getResponseCode() == 302)) {
                // Redirect. 
                String docUrl = entry.conn.getHeaderField("location");
                if (docUrl != null) {
                    this.q.offer(new CrawlUrlQueueEntry(new URLInfo(docUrl)));
                }
                logger.info(entry.info.toString() + ": skipping redirect");
                return;
            }
        } catch (IOException e) {
            return;
        }

        extract(entry);
    }

    @Override
    public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
    }

    @Override
    public Fields getSchema() {
        return this.outFields;
    }



    void extract(CrawlUrlQueueEntry entry) {
        logger.info(entry.info.toString() + ": crawed");

        if (entry.contentType == null || entry.contentType.contains("text/html")) {
            // Parse document for additional urls. 
            org.jsoup.nodes.Document doc = Jsoup.parse(entry.body, entry.info.toString());
            //Get links. 
            Elements links = doc.select("[href]");
        
            //Iterate links and print link attributes.
            for (Element e : links) {
                String link = e.attr("abs:href");
                logger.info(link + ": queued");

                // this.collector.emit(new Values<Object>(new CrawlUrlQueueEntry(new URLInfo(link))));
                this.q.offer(new CrawlUrlQueueEntry(new URLInfo(link)));
            }
        }
    }
    
}
