package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class User implements Serializable {
    String username = "";
    String hashedPwd = "";

    User(String username, String hashedPwd) {
        this.username = username;
        this.hashedPwd = hashedPwd;
    }

    String getUsername() {
        return username;
    }

    boolean hasHashedPwd(String oPwd) {
        return this.hashedPwd.equals(oPwd);
    }

    @Override
    public String toString() {
        return "[" + this.username + " " + this.hashedPwd + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        try {
            User oUser = (User) o;
            if (this.username.equals(oUser.username) && this.hashedPwd.equals(oUser.hashedPwd)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static String hashedPwd(String pwd) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        byte[] hash = digest.digest(pwd.getBytes(StandardCharsets.UTF_8));
        String hashed = Base64.getEncoder().encodeToString(hash);
        return hashed;
    }
}