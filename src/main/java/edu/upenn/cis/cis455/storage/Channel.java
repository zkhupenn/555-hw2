package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Channel implements Serializable {
    String name;
    String xpath;

    String createdByUser;
    List<String> urls = new ArrayList<>();

    public Channel(String name, String xpath, String createdByUser) {
        this.name = name;
        this.xpath = xpath;
        this.createdByUser = createdByUser;
    }
    
    public void addDoc(String url) {
        if (!this.urls.contains(url)) {
            this.urls.add(url);
        }
    }

    @Override
    public String toString() {
        return "[" + name + " " + xpath + " " + createdByUser + " " + urls.toString() + "]";
    }
    
    public String getName() {
        return name;
    }

    public String getXpath() {
        return xpath;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public List<String> getUrls() {
        return urls;
    }
}
