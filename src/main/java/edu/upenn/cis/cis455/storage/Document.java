package edu.upenn.cis.cis455.storage;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;

public class Document implements Serializable {
    int id = -1;
    String url = "";
    String content = "";
    String lastModified = null;
    String contentType = null;
    long crawledOn = System.currentTimeMillis();

    public Document(String url, String content, String modifiedSince, String contentType, long crawledOn) {
        this.url = url;
        this.content = content;
        this.lastModified = modifiedSince;
        this.contentType = contentType;
        this.crawledOn = crawledOn;
    }

    public String getUrl() {
        return url;
    }

    public String getContent() {
        return content;
    }

    public String getLastModified() {
        return lastModified;
    }

    public String getContentType() {
        return contentType;
    }

    public String getCrawledOnTimeForChannel() {
        return new SimpleDateFormat("YYYY-MM-DD'T'hh:mm:ss").format(this.crawledOn);
    }

    @Override
    public String toString() {
        return "[" + this.url + " " + this.content + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        try {
            Document oDoc = (Document) o;
            if (this.url.equals(oDoc.url) && this.content.equals(oDoc.content)) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static String md5Hash(String doc) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
        byte[] hash = digest.digest(doc.getBytes(StandardCharsets.UTF_8));
        String hashed = Base64.getEncoder().encodeToString(hash);
        return hashed;
    }
}
