package edu.upenn.cis.cis455.storage;

import java.util.Map;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public String addDocument(String url, Document doc);

    /**
     * Retrieves a document's contents by URL
     */
    public Document getDocument(String url);

    /**
     * Adds a user and returns an ID
     */
    public String addUser(String username, String password);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean getSessionForUser(String username, String password);

    /**
     * Add a new channel, getting its ID, replacing existing channel if existed. 
     */
    public String addChannel(String channelName, Channel ch);

    /**
     * Retrieves a channel contents by channel name
     */
    public Channel getChannel(String channelName);

    /**
     * Retrieves a channel contents by channel name
     */
    public Map<String, Channel> getAllChannels();

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();
}
