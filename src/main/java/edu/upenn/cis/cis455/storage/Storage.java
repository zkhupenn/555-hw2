package edu.upenn.cis.cis455.storage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Map;


public class Storage implements StorageInterface {
    private static Logger logger = LogManager.getLogger(Storage.class);
    
    private Environment dbEnv = null;
    private static final String CLASS_CATALOG = "java_class_catalog";
    private StoredClassCatalog dbCatalog;

    private String USER_STORE = "user_store";
    private Database userDb = null;
    private StoredSortedMap<String, User> userMap = null;

    private String DOCUMENT_STORE = "document_store";
    private Database docDb = null;
    private StoredSortedMap<String, Document> docMap = null;

    private String CHANNEL_STORE = "channel_store";
    private Database chDb = null;
    private StoredSortedMap<String, Channel> chMap = null;

    Storage(String path) {
        try {
            // Open database. 
            EnvironmentConfig envConfig = new EnvironmentConfig();
            envConfig.setAllowCreate(true);
            envConfig.setTransactional(true);
            this.dbEnv = new Environment(new File(path), envConfig);

            // Open tables. 
            DatabaseConfig dbConfig = new DatabaseConfig();
            dbConfig.setTransactional(true);
            dbConfig.setAllowCreate(true);

            Database catalogDb = this.dbEnv.openDatabase(null, CLASS_CATALOG, dbConfig);
            this.dbCatalog = new StoredClassCatalog(catalogDb);

            this.userDb = this.dbEnv.openDatabase(null, USER_STORE, dbConfig);
            EntryBinding<String> userKeyBinding = new SerialBinding<String>(this.getClassCatalog(), String.class);
            EntryBinding<User> userDataBinding = new SerialBinding<User>(this.getClassCatalog(), User.class);
            this.userMap = new StoredSortedMap<String, User>(this.userDb, userKeyBinding, userDataBinding, true);

            this.docDb = this.dbEnv.openDatabase(null, DOCUMENT_STORE, dbConfig);
            EntryBinding<String> docKeyBinding = new SerialBinding<String>(this.getClassCatalog(), String.class);
            EntryBinding<Document> docDataBinding = new SerialBinding<Document>(this.getClassCatalog(), Document.class);
            this.docMap = new StoredSortedMap<String, Document>(this.docDb, docKeyBinding, docDataBinding, true);

            this.chDb = this.dbEnv.openDatabase(null, CHANNEL_STORE, dbConfig);
            EntryBinding<String> chKeyBinding = new SerialBinding<String>(this.getClassCatalog(), String.class);
            EntryBinding<Channel> chDataBinding = new SerialBinding<Channel>(this.getClassCatalog(), Channel.class);
            this.chMap = new StoredSortedMap<String, Channel>(this.chDb, chKeyBinding, chDataBinding, true);
        } catch (DatabaseException dbe) {
            // Exception handling goes here
            logger.error("Error opening database " + dbe.toString());
        }
    }

    public ClassCatalog getClassCatalog() { return this.dbCatalog; }

    @Override
    public int getCorpusSize() {
        return this.docMap.size();
    }

    @Override
    public String addDocument(String url, Document doc) {
        if (this.docMap.keySet().contains(url)) {
            this.docMap.remove(url);
        }
        this.docMap.put(url, doc);
        return url;
    }

    @Override
    public Document getDocument(String url) {
        if (!this.docMap.containsKey(url)) {
            return null;
        }
        Document doc = this.docMap.get(url);
        if (doc == null) {
            return null;
        }
        return doc;
    }

    @Override
    public String addUser(String username, String password) {
        if (this.userMap.keySet().contains(username)) {
            return null;
        }
        this.userMap.put(
            username, 
            new User(username, User.hashedPwd(password))
        );
        return username;
    }

    @Override
    public boolean getSessionForUser(String username, String password) {

        User u = this.userMap.get(username);
        if (u == null) {
            return false;
        }
        return u.hasHashedPwd(User.hashedPwd(password));
    }

    @Override
    public String addChannel(String channelName, Channel ch) {
        if (this.chMap.keySet().contains(channelName)) {
            this.chMap.remove(channelName);
        }
        this.chMap.put(channelName, ch);
        return channelName;
    }

    @Override
    public Channel getChannel(String channelName) {
        return this.chMap.get(channelName);
    }

    @Override
    public Map<String, Channel> getAllChannels() {
        return this.chMap;
    }

    @Override
    public void close() {
        try{
            if (this.userDb != null) {
                this.userDb.close();
            }
            if (this.docDb != null) {
                this.docDb.close();
            }
            if (this.chDb != null) {
                this.chDb.close();
            }
            if (this.dbCatalog != null) {
                this.dbCatalog.close();
            }
            if (this.dbEnv != null) {
                this.dbEnv.cleanLog();
                this.dbEnv.close();
            }
        } catch (DatabaseException dbe) {
            // Exception handling goes here
            logger.error("Error closing environment" + dbe.toString());
        }
        
    }
    
}
