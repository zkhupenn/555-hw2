package edu.upenn.cis.cis455;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEventEngine;

public class XPathEventEngineTest {
    @Test
    public void testXPathEventEngineNoTest() throws IOException {
        XPathEventEngine eng = new XPathEventEngine();
        String[] xpaths = {"/foo/bar/xyz"};
        eng.setXPaths(xpaths);
        
        boolean[] res;
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "foo"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "zzz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "zzz"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "foo"));
        assertFalse(res[0]);
    }
    @Test
    public void testXPathEventEngineEqualTest() throws IOException {
        XPathEventEngine eng = new XPathEventEngine();
        String[] xpaths = {"/foo/bar/xyz[text() = \"eqtxt\"]"};
        eng.setXPaths(xpaths);
        
        boolean[] res;
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "foo"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "eqtxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "eqtxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "eqtxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "foo"));
        assertFalse(res[0]);
    }
    @Test
    public void testXPathEventEngineContainsTest() throws IOException {
        XPathEventEngine eng = new XPathEventEngine();
        String[] xpaths = {"/foo/bar/xyz[contains(text(), \"conttxt\")]"};
        eng.setXPaths(xpaths);
        
        boolean[] res;
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "foo"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txtcontatxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "conttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "eqconttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "conttxt"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "foo"));
        assertFalse(res[0]);
    }
    @Test
    public void testXPathEventEngineContainsAndMoreTest() throws IOException {
        XPathEventEngine eng = new XPathEventEngine();
        String[] xpaths = {"/foo/bar/xyz[contains(text(), \"conttxt\")]/abc"};
        eng.setXPaths(xpaths);
        
        boolean[] res;
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "foo"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txttxttxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "txtcontatxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "conttxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "sometxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Text, "conttxt"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc"));
        assertTrue(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "bar"));
        assertFalse(res[0]);
        res = eng.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close, "foo"));
        assertFalse(res[0]);
    }
}
