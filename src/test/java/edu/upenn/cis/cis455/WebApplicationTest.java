package edu.upenn.cis.cis455;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.handlers.HomeHandler;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;

public class WebApplicationTest {
    
    @Test
    public void testWebAppHome() throws IOException {
        String dbFolder = "testWebApplication";

        if (Files.exists(Paths.get(dbFolder))) {
            Files.walk(Paths.get(dbFolder))
            .sorted(Comparator.reverseOrder())
            .map(Path::toFile)
            .forEach(File::delete);
        }
        try {
            Files.createDirectory(Paths.get(dbFolder));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StorageInterface database = StorageFactory.getDatabaseInstance(dbFolder);
        // database.addDocument("url", new Document("url", "content", "modifiedSince", "contentType", System.currentTimeMillis()));
        database.addChannel("pages", new Channel("pages", "/html/head/title", "apple"));

        HomeHandler hh = new HomeHandler(database);
        Request req = mock(Request.class);
        String html = hh.handle(req, null);
        assertTrue(html.contains("pages"));

        Files.walk(Paths.get(dbFolder))
        .sorted(Comparator.reverseOrder())
        .map(Path::toFile)
        .forEach(File::delete);
    }
}
