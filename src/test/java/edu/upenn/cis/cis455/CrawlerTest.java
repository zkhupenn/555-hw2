package edu.upenn.cis.cis455;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.DocSet;

public class CrawlerTest {
    @Test
    public void testCrawlerCount() throws IOException {
        Crawler crawler = new Crawler("startUrl", null, 10, 1);
        assertFalse(crawler.isDone());
        crawler.incCount();
        assertTrue(crawler.isDone());
    }
    @Test
    public void testCrawlerDocSet() throws IOException {
        DocSet docSet = new DocSet();
        docSet.add("s1");
        assertTrue(docSet.contains("s1"));
        assertFalse(docSet.contains("s2"));
        assertFalse(docSet.containsAndAdd("s2"));
        assertTrue(docSet.contains("s2"));
        assertTrue(docSet.containsAndAdd("s2"));
    }
}
