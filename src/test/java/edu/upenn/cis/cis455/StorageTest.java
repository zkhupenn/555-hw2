package edu.upenn.cis.cis455;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

import org.junit.Test;

import edu.upenn.cis.cis455.storage.Document;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class StorageTest {
    
    @Test
    public void testStorageStoreDocuments() throws IOException {
        String dbFolder = "testStorageDocuments";

        if (Files.exists(Paths.get(dbFolder))) {
            Files.walk(Paths.get(dbFolder))
            .sorted(Comparator.reverseOrder())
            .map(Path::toFile)
            .forEach(File::delete);
        }
        try {
            Files.createDirectory(Paths.get(dbFolder));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StorageInterface database = StorageFactory.getDatabaseInstance(dbFolder);
        database.addDocument("url", new Document("url", "content", "modifiedSince", "contentType", System.currentTimeMillis()));
        Document d = database.getDocument("url");
        assertEquals("url", d.getUrl());
        assertEquals("content", d.getContent());
        assertEquals("modifiedSince", d.getLastModified());
        assertEquals("contentType", d.getContentType());
        
        Files.walk(Paths.get(dbFolder))
        .sorted(Comparator.reverseOrder())
        .map(Path::toFile)
        .forEach(File::delete);
    }

    @Test
    public void testStorageStoreUsers() throws IOException {
        String dbFolder = "testStorageUsers";

        if (Files.exists(Paths.get(dbFolder))) {
            Files.walk(Paths.get(dbFolder))
            .sorted(Comparator.reverseOrder())
            .map(Path::toFile)
            .forEach(File::delete);
        }
        try {
            Files.createDirectory(Paths.get(dbFolder));
        } catch (IOException e) {
            e.printStackTrace();
        }

        StorageInterface database = StorageFactory.getDatabaseInstance(dbFolder);
        database.addUser("username", "password");
        assertTrue(database.getSessionForUser("username", "password"));
        assertFalse(database.getSessionForUser("username", "passwordd"));
        assertFalse(database.getSessionForUser("usernamee", "password"));
        
        Files.walk(Paths.get(dbFolder))
        .sorted(Comparator.reverseOrder())
        .map(Path::toFile)
        .forEach(File::delete);
    }
}
