package edu.upenn.cis.cis455;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.CrawlUrlQueueEntry;
import edu.upenn.cis.cis455.crawler.CrawlerUrlQueue;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class CrawlerUrlQueueTest {
    @Test
    public void testCrawlerUrlQueue() throws IOException {
        CrawlerUrlQueue q = new CrawlerUrlQueue();
        q.offer(new CrawlUrlQueueEntry(new URLInfo("a", "a")));
        q.offer(new CrawlUrlQueueEntry(new URLInfo("b", "b")));
        q.offer(new CrawlUrlQueueEntry(new URLInfo("c", "c")));
        q.offer(new CrawlUrlQueueEntry(new URLInfo("d", "d")));
        assertEquals("a", q.poll().info.getHostName());
        assertEquals("b", q.poll().info.getHostName());
        assertEquals("c", q.poll().info.getHostName());
        assertEquals("d", q.poll().info.getHostName());
        assertNull(q.poll());
    }
    
}
